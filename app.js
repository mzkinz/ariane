const express = require('express');
const app = express();
const port = 3000;

app.get('/', (req, res) => {
  res.send('hello, my name is Ariane');
});

app.get('/test', function (req, res) {
  res.send('{ "response": "Testing Ariane" }');
});

app.listen(port, () => {
  console.log(`Server running at http://localhost:${port}/`);
});

module.exports = app;
