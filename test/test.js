var request = require('supertest');
var app = require('../app.js');
describe('GET /test', function() {
    it('respond with Testing Ariane', function(done) {
        request(app).get('/test').expect('{ "response": "Testing Ariane" }', done);
    });
});
